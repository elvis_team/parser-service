package com.codechallenge.parserservice.model;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("tracker_columnfamily")
public class Tracker {

	@PrimaryKey private TrackerKey key;

	@Column("advertisement_id")
	private Integer advertiseId;
  
  	private String site;
  	
  	@Column("timestamp")
	private Date tyme;

	public Tracker(TrackerKey key, Integer advertiseId, String site, Date tyme) {
		this.key = key;
		this.advertiseId = advertiseId;
		this.site = site;
		this.tyme = tyme;
	}

	public TrackerKey getKey() {
		return key;
	}
	
	public void setKey(TrackerKey key) {
		this.key = key;
	}
	
	public String getSite() {
		return site;
	}
	
	public void setSite(String site) {
		this.site = site;
	}

	public Integer getAdvertiseId() {
		return advertiseId;
	}

	public void setAdvertiseId(Integer advertiseId) {
		this.advertiseId = advertiseId;
	}

	public Date getTyme() {
		return tyme;
	}

	public void setTyme(Date tyme) {
		this.tyme = tyme;
	}
  
}
