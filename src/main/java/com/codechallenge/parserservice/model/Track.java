package com.codechallenge.parserservice.model;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("track_columnfamily")
public class Track {

	@PrimaryKey private TrackKey key;

	@Column("timestamp")
	private Date tyme;
  
	public Track(TrackKey key, Date tyme) {
		this.key = key;
		this.tyme = tyme;
	}

	public TrackKey getKey() {
		return key;
	}
	
	public void setKey(TrackKey key) {
		this.key = key;
	}

	public Date getTyme() {
		return tyme;
	}

	public void setTyme(Date tyme) {
		this.tyme = tyme;
	}
	
}
