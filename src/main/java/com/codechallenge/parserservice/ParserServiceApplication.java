package com.codechallenge.parserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * This class listens for ingestion events, parses it, and stores it in a cassandra dB
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ParserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParserServiceApplication.class, args);
	}
}
