package com.codechallenge.parserservice.config;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories
public class CassandraConfig extends AbstractCassandraConfiguration {

  @Value("${cassandra.contactpoints}")
  private String contactPoints;

  @Value("${cassandra.port}")
  private int port;

  @Value("${cassandra.keyspace}")
  private String keySpace;

  @Value("${cassandra.basepackages}")
  private String basePackages;

  @Override
  protected String getKeyspaceName() {
    return keySpace;
  }

  @Override
  protected String getContactPoints() {
    return contactPoints;
  }

  @Override
  protected int getPort() {
    return port;
  }

  @Override
  public SchemaAction getSchemaAction() {
    return SchemaAction.CREATE_IF_NOT_EXISTS;
  }

  @Override
  public String[] getEntityBasePackages() {
    return new String[] {basePackages};
  }
  
  @Override
  protected List getKeyspaceCreations() {
	  return Collections.singletonList(CreateKeyspaceSpecification
			  .createKeyspace(keySpace).ifNotExists()
			  .with(KeyspaceOption.DURABLE_WRITES, true)
			  .withSimpleReplication(3));
  }
  
//  @Override
//  protected List getStartupScripts() {
//	  return Collections.singletonList("CREATE KEYSPACE IF NOT EXISTS "
//			  + keySpace + " WITH replication = {"
//			  + " 'class': 'SimpleStrategy', "
//			  + " 'replication_factor': '3' " + "};");
//	  
//  }
  
}
