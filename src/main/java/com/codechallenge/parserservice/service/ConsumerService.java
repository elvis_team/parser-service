package com.codechallenge.parserservice.service;

import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.codechallenge.parserservice.config.Configuration;
import com.codechallenge.parserservice.model.AdDTO;
import com.codechallenge.parserservice.model.Track;
import com.codechallenge.parserservice.model.TrackKey;
import com.codechallenge.parserservice.model.Tracker;
import com.codechallenge.parserservice.model.TrackerKey;
import com.codechallenge.parserservice.repository.TrackRepository;
import com.codechallenge.parserservice.repository.TrackerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class ConsumerService {

	private final Logger log = LoggerFactory.getLogger(ConsumerService.class);
	private static ObjectMapper mapper = null;
	
	@Autowired
	private Configuration configuration;
	
	@Autowired
	private TrackerRepository trackerRepository;
	
	@Autowired
	private TrackRepository trackRepository;
	
	@KafkaListener(topics="${parser-service.deliveryeventtopic}")
    public void pushDeliverEvent(String json) {
		log.info("*** A received {} content = '{}'", configuration.getDeliveryeventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackerKey trackerKey = new TrackerKey("View", adDomain.getTime(), adDomain.getBrowser(), adDomain.getOs(), adDomain.getDeliveryId());
			Tracker tracker = new Tracker(trackerKey, adDomain.getAdvertisementId(), adDomain.getSite(), new Date());
			trackerRepository.insert(tracker);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} exception occurred {}", configuration.getDeliveryeventtopic(), e.getMessage());
		}
    }
	
	@KafkaListener(topics="${parser-service.deliveryeventtopic}", groupId = "track")
	public void pushDeliverEventTrack(String json) {
		log.info("*** track received {} content = '{}'", configuration.getDeliveryeventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackKey trackerKey = new TrackKey("View", adDomain.getDeliveryId(), adDomain.getTime());
			Track track = new Track(trackerKey, new Date());
			trackRepository.insert(track);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} track exception occurred {}", configuration.getDeliveryeventtopic(), e.getMessage());
		}
	}
	
	@KafkaListener(topics="${parser-service.clickeventtopic}")
	public void pushClickEvent(String json) {
		log.info("*** received {} content = '{}'", configuration.getClickeventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackerKey trackerKey = new TrackerKey("Click", adDomain.getTime(), adDomain.getBrowser(), adDomain.getOs(), adDomain.getClickId());
			Tracker tracker = new Tracker(trackerKey, adDomain.getAdvertisementId(), adDomain.getSite(), new Date());
			trackerRepository.insert(tracker);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} exception occurred {}", configuration.getClickeventtopic(), e.getMessage());
		}
	}
	
	@KafkaListener(topics="${parser-service.clickeventtopic}", groupId = "track")
	public void pushClickEventTrack(String json) {
		log.info("*** track received {} content = '{}'", configuration.getClickeventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackKey trackerKey = new TrackKey("Click", adDomain.getClickId(), adDomain.getTime());
			Track track = new Track(trackerKey, new Date());
			trackRepository.insert(track);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} track exception occurred {}", configuration.getClickeventtopic(), e.getMessage());
		}
	}
	
	@KafkaListener(topics="${parser-service.installeventtopic}")
	public void pushInstallEvent(String json) {
		log.info("*** received {} content = '{}'", configuration.getInstalleventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackerKey trackerKey = new TrackerKey("Install", adDomain.getTime(), adDomain.getBrowser(), adDomain.getOs(), adDomain.getInstallId());
			Tracker tracker = new Tracker(trackerKey, adDomain.getAdvertisementId(), adDomain.getSite(), new Date());
			trackerRepository.insert(tracker);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} exception occurred {}", configuration.getInstalleventtopic(), e.getMessage());
		}
	}
	
	@KafkaListener(topics="${parser-service.installeventtopic}", groupId = "track")
	public void pushInstallEventTrack(String json) {
		log.info("*** track received {} content = '{}'", configuration.getInstalleventtopic(), json);
		//objectify and push content to database
		try {
			AdDTO adDomain = getSingleMapper().readValue(json, AdDTO.class);
			TrackKey trackerKey = new TrackKey("Install", adDomain.getInstallId(), adDomain.getTime());
			Track track = new Track(trackerKey, new Date());
			trackRepository.insert(track);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("*** Oops! {} track exception occurred {}", configuration.getInstalleventtopic(), e.getMessage());
		}
	}
	
	public ObjectMapper getSingleMapper() {
       if(mapper == null) {
    	   log.info("Returning new Object Mapper...");
    	   mapper = new ObjectMapper();
       } else {
    	   log.info("Returning Object Mapper...");
       }
       return mapper;
    }
	
}
