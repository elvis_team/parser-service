package com.codechallenge.parserservice.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.codechallenge.parserservice.model.Track;
import com.codechallenge.parserservice.model.TrackKey;


@Repository
public interface TrackRepository extends CassandraRepository<Track, TrackKey> {

}
