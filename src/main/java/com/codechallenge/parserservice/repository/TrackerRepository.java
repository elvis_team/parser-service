package com.codechallenge.parserservice.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.codechallenge.parserservice.model.Tracker;
import com.codechallenge.parserservice.model.TrackerKey;


@Repository
public interface TrackerRepository extends CassandraRepository<Tracker, TrackerKey> {

}
